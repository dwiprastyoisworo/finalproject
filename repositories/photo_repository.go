package repositories

import (
	"finalProject/models"
	"gorm.io/gorm"
)

type IPhotoRepository interface {
	Create(db *gorm.DB, photo *models.Photo) error
	Get(db *gorm.DB, userId uint, photo *[]models.Photo) error
	GetPhotoByID(db *gorm.DB, userId uint, photoId uint, photo *models.Photo) error
	Update(db *gorm.DB, photo *models.Photo) error
	Delete(db *gorm.DB, photo *models.Photo) error
}

type PhotoRepository struct {
}

func NewPhotoRepository() IPhotoRepository {
	return &PhotoRepository{}
}

func (s *PhotoRepository) Create(db *gorm.DB, photo *models.Photo) error {
	err := db.Create(photo).Error
	return err
}

func (s *PhotoRepository) Get(db *gorm.DB, userId uint, photos *[]models.Photo) error {
	err := db.Preload("User").Where("user_id", userId).Find(photos).Error
	return err
}

func (s *PhotoRepository) GetPhotoByID(db *gorm.DB, userId uint, photoId uint, photo *models.Photo) error {
	err := db.Preload("User").Where("user_id = ?", userId).Where("id = ?", photoId).First(photo).Error
	return err
}

func (s *PhotoRepository) Update(db *gorm.DB, photo *models.Photo) error {
	err := db.Save(photo).Error
	return err
}

func (s *PhotoRepository) Delete(db *gorm.DB, photo *models.Photo) error {
	err := db.Delete(photo).Error
	return err
}
