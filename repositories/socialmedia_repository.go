package repositories

import (
	"finalProject/models"
	"gorm.io/gorm"
)

type ISocialmediaRepository interface {
	Create(db *gorm.DB, socialmedia *models.Socialmedia) error
	Get(db *gorm.DB, userId uint, socialmedia *[]models.Socialmedia) error
	GetByID(db *gorm.DB, userId uint, socialmediaId uint, socialmedia *models.Socialmedia) error
	Update(db *gorm.DB, socialmedia *models.Socialmedia) error
	Delete(db *gorm.DB, socialmedia *models.Socialmedia) error
}

type SocialmediaRepository struct {
}

func NewSocialmediaRepository() ISocialmediaRepository {
	return &SocialmediaRepository{}
}

func (s *SocialmediaRepository) Create(db *gorm.DB, socialmedia *models.Socialmedia) error {
	err := db.Create(socialmedia).Error
	return err
}

func (s *SocialmediaRepository) Get(db *gorm.DB, userId uint, socialmedias *[]models.Socialmedia) error {
	err := db.Preload("User").Where("user_id", userId).Find(socialmedias).Error
	return err
}

func (s *SocialmediaRepository) GetByID(db *gorm.DB, userId uint, socialmediaId uint, socialmedia *models.Socialmedia) error {
	err := db.Preload("User").Where("user_id = ?", userId).Where("id = ?", socialmediaId).First(socialmedia).Error
	return err
}

func (s *SocialmediaRepository) Update(db *gorm.DB, socialmedia *models.Socialmedia) error {
	err := db.Save(socialmedia).Error
	return err
}

func (s *SocialmediaRepository) Delete(db *gorm.DB, socialmedia *models.Socialmedia) error {
	err := db.Delete(socialmedia).Error
	return err
}
