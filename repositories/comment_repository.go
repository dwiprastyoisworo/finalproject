package repositories

import (
	"finalProject/models"
	"gorm.io/gorm"
)

type ICommentRepository interface {
	Create(db *gorm.DB, comment *models.Comment) error
	Get(db *gorm.DB, userId *uint, comment []*models.Comment) error
	GetCommentByID(db *gorm.DB, userId *uint, commentId *uint, comment *models.Comment) error
	Update(db *gorm.DB, comment *models.Comment) error
	Delete(db *gorm.DB, comment *models.Comment) error
}

type CommentRepository struct {
}

func NewCommentRepository() ICommentRepository {
	return &CommentRepository{}
}

func (s *CommentRepository) Create(db *gorm.DB, comment *models.Comment) error {
	err := db.Create(comment).Error
	return err
}

func (s *CommentRepository) Get(db *gorm.DB, userId *uint, comments []*models.Comment) error {
	err := db.Preload("User").Preload("Photo").Where("user_id", userId).Find(comments).Error
	return err
}

func (s *CommentRepository) GetCommentByID(db *gorm.DB, userId *uint, commentId *uint, comment *models.Comment) error {
	err := db.Preload("User").Where("user_id = ?", userId).Where("id = ?", commentId).First(comment).Error
	return err
}

func (s *CommentRepository) Update(db *gorm.DB, comment *models.Comment) error {
	err := db.Save(comment).Error
	return err
}

func (s *CommentRepository) Delete(db *gorm.DB, comment *models.Comment) error {
	err := db.Delete(comment).Error
	return err
}
