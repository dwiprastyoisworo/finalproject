package repositories

import (
	"finalProject/models"
	"gorm.io/gorm"
)

type IUserRepository interface {
	Insert(db *gorm.DB, user *models.User) error
	UpdateUser(db *gorm.DB, id uint, user *models.User) error
	DeleteUser(db *gorm.DB, user *models.User) error
	GetUserByEmail(db *gorm.DB, email string, user *models.User) error
	GetUserByID(db *gorm.DB, id uint, user *models.User) error
}

type UserRepository struct {
}

func NewUserRepository() IUserRepository {
	return &UserRepository{}
}

func (repository *UserRepository) Insert(db *gorm.DB, user *models.User) error {
	err := db.Create(user).Error
	return err
}

func (repository *UserRepository) UpdateUser(db *gorm.DB, id uint, user *models.User) error {
	err := db.Save(user).Error
	return err
}

func (repository *UserRepository) DeleteUser(db *gorm.DB, user *models.User) error {
	err := db.Delete(user).Error
	return err
}

func (repository *UserRepository) GetUserByEmail(db *gorm.DB, email string, user *models.User) error {
	err := db.Where("email = ?", email).First(user).Error
	return err
}

func (repository *UserRepository) GetUserByID(db *gorm.DB, id uint, user *models.User) error {
	err := db.Where("id = ?", id).First(user).Error
	return err
}
