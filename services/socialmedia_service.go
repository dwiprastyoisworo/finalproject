package services

import (
	"finalProject/models"
	"finalProject/repositories"
	"gorm.io/gorm"
)

type ISocialmediaService interface {
	Create(userId uint, socialmedia *models.Socialmedia) error
	Get(userId uint, socialmedias *[]models.Socialmedia) error
	Update(userId uint, socialmediaId uint, socialmedia *models.Socialmedia) (models.Socialmedia, error)
	Delete(userId uint, socialmediaid uint) error
}

type SocialmediaService struct {
	userRepository        repositories.IUserRepository
	socialmediaRepository repositories.ISocialmediaRepository
	db                    *gorm.DB
}

func NewSocialmediaService(userRepository repositories.IUserRepository, socialmediaRepository repositories.ISocialmediaRepository, db *gorm.DB) *SocialmediaService {
	return &SocialmediaService{userRepository: userRepository, socialmediaRepository: socialmediaRepository, db: db}
}

func (s *SocialmediaService) Create(userId uint, socialmedia *models.Socialmedia) error {
	var user models.User
	err := s.userRepository.GetUserByID(s.db, userId, &user)
	if err != nil {
		return err
	}
	socialmedia.User = user

	err = s.socialmediaRepository.Create(s.db, socialmedia)
	if err != nil {
		return err
	}
	return nil
}

func (s *SocialmediaService) Get(userId uint, socialmedias *[]models.Socialmedia) error {
	err := s.socialmediaRepository.Get(s.db, userId, socialmedias)
	if err != nil {
		return err
	}
	return nil
}

func (s *SocialmediaService) Update(userId uint, socialmediaId uint, paramSocialmedia *models.Socialmedia) (models.Socialmedia, error) {
	var socialmedia models.Socialmedia
	err := s.socialmediaRepository.GetByID(s.db, userId, socialmediaId, &socialmedia)
	if err != nil {
		return socialmedia, err
	}

	if paramSocialmedia.Name != "" {
		socialmedia.Name = paramSocialmedia.Name
	}

	if paramSocialmedia.SocialMediaUrl != "" {
		socialmedia.SocialMediaUrl = paramSocialmedia.SocialMediaUrl
	}

	err = s.socialmediaRepository.Update(s.db, &socialmedia)
	if err != nil {
		return socialmedia, err
	}

	return socialmedia, nil
}

func (s *SocialmediaService) Delete(userId uint, socialmediaId uint) error {
	var socialmedia models.Socialmedia
	err := s.socialmediaRepository.GetByID(s.db, userId, socialmediaId, &socialmedia)
	if err != nil {
		return err
	}

	err = s.socialmediaRepository.Delete(s.db, &socialmedia)
	if err != nil {
		return err
	}
	return nil
}
