package services

import (
	"finalProject/models"
	"finalProject/repositories"
	"gorm.io/gorm"
)

type ICommentService interface {
	Create(userId uint, comment *models.Comment) error
	Get(userId *uint, comments []*models.Comment) error
	Update(userId *uint, commentId *uint, comment *models.Comment) (models.Comment, error)
	Delete(userId *uint, commentid *uint) error
}

type CommentService struct {
	userRepository    repositories.IUserRepository
	commentRepository repositories.ICommentRepository
	db                *gorm.DB
}

func NewCommentService(userRepository repositories.IUserRepository, commentRepository repositories.ICommentRepository, db *gorm.DB) *CommentService {
	return &CommentService{userRepository: userRepository, commentRepository: commentRepository, db: db}
}

func (s *CommentService) Create(userId uint, comment *models.Comment) error {
	var user models.User
	err := s.userRepository.GetUserByID(s.db, userId, &user)
	if err != nil {
		return err
	}
	comment.User = user

	err = s.commentRepository.Create(s.db, comment)
	if err != nil {
		return err
	}
	return nil
}

func (s *CommentService) Get(userId *uint, comments []*models.Comment) error {
	err := s.commentRepository.Get(s.db, userId, comments)
	if err != nil {
		return err
	}
	return nil
}

func (s *CommentService) Update(userId *uint, commentId *uint, paramComment *models.Comment) (models.Comment, error) {
	var comment models.Comment
	err := s.commentRepository.GetCommentByID(s.db, userId, commentId, &comment)
	if err != nil {
		return comment, err
	}

	if paramComment.Message != "" {
		comment.Message = paramComment.Message
	}

	err = s.commentRepository.Update(s.db, &comment)
	if err != nil {
		return comment, err
	}

	return comment, nil
}

func (s *CommentService) Delete(userId *uint, commentId *uint) error {
	var comment models.Comment
	err := s.commentRepository.GetCommentByID(s.db, userId, commentId, &comment)
	if err != nil {
		return err
	}

	err = s.commentRepository.Delete(s.db, &comment)
	if err != nil {
		return err
	}
	return nil
}
