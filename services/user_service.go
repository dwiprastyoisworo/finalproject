package services

import (
	"errors"
	"finalProject/helpers"
	"finalProject/models"
	"finalProject/repositories"
	"gorm.io/gorm"
)

type IUserService interface {
	Register(user *models.User) (models.User, error)
	Login(user *models.User) (string, error)
	Update(id uint, user *models.User) (models.User, error)
	Delete(id uint) error
}

type UserService struct {
	userRepository repositories.IUserRepository
	db             *gorm.DB
}

func NewUserService(userRepository repositories.IUserRepository, db *gorm.DB) *UserService {
	return &UserService{userRepository: userRepository, db: db}
}

func (service *UserService) Register(user *models.User) (models.User, error) {
	err := service.userRepository.Insert(service.db, user)
	if err != nil {
		return models.User{}, err
	}
	return *user, err
}

func (service *UserService) Login(user *models.User) (string, error) {
	var token string
	var userModel models.User
	err := service.userRepository.GetUserByEmail(service.db, user.Email, &userModel)
	if err != nil {
		return token, err
	}
	comparePass := helpers.ComparePass([]byte(userModel.Password), []byte(user.Password))
	if !comparePass {
		return token, errors.New("Unauthorized")
	}

	token = helpers.GenerateToken(userModel.ID, userModel.Email)

	return token, nil
}

func (service *UserService) Update(id uint, paramUser *models.User) (models.User, error) {
	var user models.User
	err := service.userRepository.GetUserByID(service.db, id, &user)
	if err != nil {
		return user, err
	}

	if paramUser.Email != "" {
		user.Email = paramUser.Email
	}

	if paramUser.Username != "" {
		user.Username = paramUser.Username
	}

	err = service.userRepository.UpdateUser(service.db, user.ID, &user)
	if err != nil {
		return user, err
	}

	return *paramUser, nil
}

func (service *UserService) Delete(id uint) error {
	var user models.User
	err := service.userRepository.GetUserByID(service.db, id, &user)
	if err != nil {
		return err
	}

	err = service.userRepository.DeleteUser(service.db, &user)
	if err != nil {
		return err
	}
	return nil
}
