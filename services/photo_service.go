package services

import (
	"finalProject/models"
	"finalProject/repositories"
	"gorm.io/gorm"
)

type IPhotoService interface {
	Create(userId uint, photo *models.Photo) error
	Get(userId uint, photos *[]models.Photo) error
	Update(userId uint, photoId uint, photo *models.Photo) (models.Photo, error)
	Delete(userId uint, photoId uint) error
}

type PhotoService struct {
	userRepository  repositories.IUserRepository
	photoRepository repositories.IPhotoRepository
	db              *gorm.DB
}

func NewPhotoService(userRepository repositories.IUserRepository, photoRepository repositories.IPhotoRepository, db *gorm.DB) *PhotoService {
	return &PhotoService{userRepository: userRepository, photoRepository: photoRepository, db: db}
}

func (s *PhotoService) Create(userId uint, photo *models.Photo) error {
	var user models.User
	err := s.userRepository.GetUserByID(s.db, userId, &user)
	if err != nil {
		return err
	}
	photo.User = user

	err = s.photoRepository.Create(s.db, photo)
	if err != nil {
		return err
	}
	return nil
}

func (s *PhotoService) Get(userId uint, photos *[]models.Photo) error {
	err := s.photoRepository.Get(s.db, userId, photos)
	if err != nil {
		return err
	}
	return nil
}

func (s *PhotoService) Update(userId uint, photoId uint, photo *models.Photo) (models.Photo, error) {
	var photoModel = models.Photo{}
	err := s.photoRepository.GetPhotoByID(s.db, userId, photoId, &photoModel)
	if err != nil {
		return models.Photo{}, err
	}

	if photo.Title != "" {
		photoModel.Title = photo.Title
	}

	if photo.Caption != "" {
		photoModel.Caption = photo.Caption
	}

	if photo.PhotoUrl != "" {
		photoModel.PhotoUrl = photo.PhotoUrl
	}

	err = s.photoRepository.Update(s.db, &photoModel)
	if err != nil {
		return models.Photo{}, err
	}

	return *photo, nil
}

func (s *PhotoService) Delete(userId uint, photoId uint) error {
	var photo models.Photo
	err := s.photoRepository.GetPhotoByID(s.db, userId, photoId, &photo)
	if err != nil {
		return err
	}

	err = s.photoRepository.Delete(s.db, &photo)
	if err != nil {
		return err
	}
	return nil
}
