package models

import (
	"time"
)

type Comment struct {
	GormModel
	Message string `json:"message" form:"message" valid:"required~Message is required"`
	UserID  uint   `json:"user_id"`
	User    User
	PhotoID uint `json:"photo_id"`
	Photo   Photo
}

type CommentResponse struct {
	ID        uint          `json:"id"`
	Message   string        `json:"message"`
	UserID    uint          `json:"user_id"`
	PhotoID   uint          `json:"photo_id"`
	CreatedAt *time.Time    `json:"created_at"`
	UpdatedAt *time.Time    `json:"updated_at"`
	User      UserResponse  `json:"user"`
	Photo     PhotoResponse `json:"photo"`
}

type CreateCommentResponse struct {
	ID        uint       `json:"id"`
	Message   string     `json:"message"`
	UserID    uint       `json:"user_id"`
	PhotoID   uint       `json:"photo_id"`
	CreatedAt *time.Time `json:"created_at"`
}

type UpdateCommentResponse struct {
	ID        uint       `json:"id"`
	Message   string     `json:"message"`
	UserID    uint       `json:"user_id"`
	PhotoID   uint       `json:"photo_id"`
	UpdatedAt *time.Time `json:"updated_at"`
}

func (u *Comment) CommentToCommentResponse() CommentResponse {
	return CommentResponse{
		ID:        u.ID,
		Message:   u.Message,
		UserID:    u.UserID,
		PhotoID:   u.PhotoID,
		CreatedAt: u.CreatedAt,
		UpdatedAt: u.UpdatedAt,
	}
}

func (u *Comment) CommentToCreateCommentResponse() CreateCommentResponse {
	return CreateCommentResponse{
		ID:        u.ID,
		Message:   u.Message,
		UserID:    u.UserID,
		PhotoID:   u.PhotoID,
		CreatedAt: u.CreatedAt,
	}
}

func (u *Comment) CommentToUpdateCommentResponse() UpdateCommentResponse {
	return UpdateCommentResponse{
		ID:        u.ID,
		Message:   u.Message,
		UserID:    u.UserID,
		PhotoID:   u.PhotoID,
		UpdatedAt: u.UpdatedAt,
	}
}

func (u *Comment) CommentToCommentDetailResponse() CommentResponse {
	return CommentResponse{
		ID:        u.ID,
		Message:   u.Message,
		UserID:    u.UserID,
		PhotoID:   u.PhotoID,
		CreatedAt: u.CreatedAt,
		UpdatedAt: u.UpdatedAt,
		User: UserResponse{
			ID:       u.User.ID,
			Username: u.User.Username,
			Email:    u.User.Email,
		},
		Photo: PhotoResponse{
			ID:       u.Photo.ID,
			Title:    u.Photo.Title,
			Caption:  u.Photo.Caption,
			PhotoUrl: u.Photo.PhotoUrl,
			UserID:   u.Photo.UserID,
		},
	}
}
