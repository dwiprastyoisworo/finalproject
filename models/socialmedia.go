package models

import (
	"time"

	"github.com/asaskevich/govalidator"
	"gorm.io/gorm"
)

type Socialmedia struct {
	GormModel
	Name           string `json:"name" form:"name" valid:"required~Name is required"`
	SocialMediaUrl string `json:"social_media_url" form:"social_media_url" valid:"required~Social media url is required"`
	UserID         uint
	User           User
}

type SocialmediaResponse struct {
	ID             uint         `json:"id"`
	Name           string       `json:"name"`
	SocialMediaUrl string       `json:"social_media_url"`
	UserID         uint         `json:"user_id"`
	CreatedAt      *time.Time   `json:"created_at"`
	UpdatedAt      *time.Time   `json:"updated_at"`
	User           UserResponse `json:"user"`
}

type CreateSocialmediaResponse struct {
	ID             uint       `json:"id"`
	Name           string     `json:"name"`
	SocialMediaUrl string     `json:"social_media_url"`
	UserID         uint       `json:"user_id"`
	CreatedAt      *time.Time `json:"created_at"`
}

type UpdateSocialmediaResponse struct {
	ID             uint       `json:"id"`
	Name           string     `json:"name"`
	SocialMediaUrl string     `json:"social_media_url"`
	UserID         uint       `json:"user_id"`
	UpdatedAt      *time.Time `json:"updated_at"`
}

func (u *Socialmedia) SocialmediaToSocialmediaResponse() SocialmediaResponse {
	return SocialmediaResponse{
		ID:             u.ID,
		Name:           u.Name,
		SocialMediaUrl: u.SocialMediaUrl,
		UserID:         u.UserID,
		CreatedAt:      u.CreatedAt,
		UpdatedAt:      u.UpdatedAt,
		User:           u.User.UserToUser(),
	}
}

func (u *Socialmedia) SocialmediaToCreateSocialmediaResponse() CreateSocialmediaResponse {
	return CreateSocialmediaResponse{
		ID:             u.ID,
		Name:           u.Name,
		SocialMediaUrl: u.SocialMediaUrl,
		UserID:         u.UserID,
		CreatedAt:      u.CreatedAt,
	}
}

func (u *Socialmedia) SocialmediaToUpdateSocialmediaResponse() UpdateSocialmediaResponse {
	return UpdateSocialmediaResponse{
		ID:             u.ID,
		Name:           u.Name,
		SocialMediaUrl: u.SocialMediaUrl,
		UserID:         u.UserID,
		UpdatedAt:      u.UpdatedAt,
	}
}

func (p *Socialmedia) BeforeCreate(tx *gorm.DB) (err error) {
	_, errCreate := govalidator.ValidateStruct(p)
	if errCreate != nil {
		err = errCreate
		return
	}

	err = nil
	return
}

func (p *Socialmedia) BeforeUpdate(tx *gorm.DB) (err error) {
	_, errUpdate := govalidator.ValidateStruct(p)
	if errUpdate != nil {
		err = errUpdate
		return
	}

	err = nil
	return
}
