package models

import (
	"time"

	"github.com/asaskevich/govalidator"
	"gorm.io/gorm"
)

type Photo struct {
	GormModel
	Title    string `json:"title" form:"title" valid:"required~Title is required"`
	Caption  string `json:"caption" form:"caption"`
	PhotoUrl string `json:"photo_url" form:"photo_url" valid:"required~Caption is required"`
	UserID   uint
	User     User
	Comment  []Comment `gorm:"foreignKey:PhotoID;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;" json:"comment" form:"comment"`
}

type CreatePhotoResponse struct {
	ID        uint       `json:"id"`
	Title     string     `json:"title"`
	Caption   string     `json:"caption"`
	PhotoUrl  string     `json:"photo_url"`
	UserID    uint       `json:"user_id"`
	CreatedAt *time.Time `json:"created_at"`
}

type UpdatePhotoResponse struct {
	ID        uint       `json:"id"`
	Title     string     `json:"title"`
	Caption   string     `json:"caption"`
	PhotoUrl  string     `json:"photo_url"`
	UserID    uint       `json:"user_id"`
	UpdatedAt *time.Time `json:"created_at"`
}

type PhotoResponse struct {
	ID        uint         `json:"id"`
	Title     string       `json:"title"`
	Caption   string       `json:"caption"`
	PhotoUrl  string       `json:"photo_url"`
	UserID    uint         `json:"user_id"`
	CreatedAt *time.Time   `json:"created_at"`
	UpdatedAt *time.Time   `json:"updated_at"`
	User      UserResponse `json:"user"`
}

func (u *Photo) PhotoToPhotoResponse() PhotoResponse {
	return PhotoResponse{
		ID:        u.ID,
		Title:     u.Title,
		Caption:   u.Caption,
		PhotoUrl:  u.PhotoUrl,
		UserID:    u.UserID,
		CreatedAt: u.CreatedAt,
		UpdatedAt: u.UpdatedAt,
		User:      u.User.UserToUser(),
	}
}

func (u *Photo) PhotoToCreatePhotoResponse() CreatePhotoResponse {
	return CreatePhotoResponse{
		ID:        u.ID,
		Title:     u.Title,
		Caption:   u.Caption,
		PhotoUrl:  u.PhotoUrl,
		UserID:    u.UserID,
		CreatedAt: u.CreatedAt,
	}
}

func (u *Photo) PhotoToUpdatePhotoResponse() UpdatePhotoResponse {
	return UpdatePhotoResponse{
		ID:        u.ID,
		Title:     u.Title,
		Caption:   u.Caption,
		PhotoUrl:  u.PhotoUrl,
		UserID:    u.UserID,
		UpdatedAt: u.UpdatedAt,
	}
}

func (p *Photo) BeforeCreate(tx *gorm.DB) (err error) {
	_, errCreate := govalidator.ValidateStruct(p)
	if errCreate != nil {
		err = errCreate
		return
	}

	err = nil
	return
}

func (p *Photo) BeforeUpdate(tx *gorm.DB) (err error) {
	_, errUpdate := govalidator.ValidateStruct(p)
	if errUpdate != nil {
		err = errUpdate
		return
	}

	err = nil
	return
}
