package middleware

import (
	"finalProject/helpers"
	"finalProject/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

func BasicAuth(c *gin.Context) {
	user, password, hasAuth := c.Request.BasicAuth()
	isValid := hasAuth && user == utils.GetEnv("BASIC_AUTH_USERNAME") && password == utils.GetEnv("BASIC_AUTH_PASSWORD")
	if !isValid {
		helpers.Response(c, http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized), nil)
	}
}
