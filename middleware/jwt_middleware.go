package middleware

import (
	"finalProject/helpers"
	"net/http"

	"github.com/gin-gonic/gin"
)

func JWTAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		verifyToken, err := helpers.VerifyToken(c)

		if err != nil {
			c.Abort()
			helpers.Response(c, http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized), nil)
			return
		}

		c.Set("userData", verifyToken)
		c.Next()
	}
}
