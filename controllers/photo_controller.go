package controllers

import (
	"finalProject/helpers"
	"finalProject/models"
	"finalProject/services"
	"fmt"
	"net/http"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type PhotoController struct {
	photoService services.IPhotoService
}

func NewPhotoController(photoService services.IPhotoService) *PhotoController {
	return &PhotoController{photoService: photoService}
}

func (s *PhotoController) Create(c *gin.Context) {
	var photo models.Photo
	userData := c.MustGet("userData").(jwt.MapClaims)
	floatUserId := userData["id"].(float64)
	userId := uint(floatUserId)

	if err := c.ShouldBindJSON(&photo); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	err := s.photoService.Create(userId, &photo)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}

	helpers.Response(c, http.StatusOK, http.StatusText(http.StatusOK), photo.PhotoToCreatePhotoResponse())
}

func (s *PhotoController) Get(c *gin.Context) {
	userData := c.MustGet("userData").(jwt.MapClaims)
	floatUserId := userData["id"].(float64)
	userId := uint(floatUserId)

	var photos []models.Photo
	err := s.photoService.Get(userId, &photos)
	if err != nil {
		helpers.Response(c, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		return
	}

	var responsePhoto []models.PhotoResponse
	for _, k := range photos {
		responsePhoto = append(responsePhoto, k.PhotoToPhotoResponse())
	}
	helpers.Response(c, http.StatusOK, http.StatusText(http.StatusOK), responsePhoto)
}

func (s *PhotoController) Update(c *gin.Context) {
	userData := c.MustGet("userData").(jwt.MapClaims)
	floatUserId := userData["id"].(float64)
	userId := uint(floatUserId)

	tmpPhotoId, err := strconv.ParseFloat(c.Param("photoId"), 64)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}
	photoId := uint(tmpPhotoId)
	fmt.Println(photoId)
	var photo models.Photo
	if err := c.ShouldBindJSON(&photo); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	photo, err = s.photoService.Update(userId, photoId, &photo)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}

	helpers.Response(c, http.StatusOK, http.StatusText(http.StatusOK), photo.PhotoToUpdatePhotoResponse())
}

func (s *PhotoController) Delete(c *gin.Context) {
	userData := c.MustGet("userData").(jwt.MapClaims)
	floatUserId := userData["id"].(float64)
	userId := uint(floatUserId)

	tmpPhotoId, err := strconv.ParseFloat(c.Param("photoId"), 64)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}
	photoId := uint(tmpPhotoId)

	err = s.photoService.Delete(userId, photoId)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}
	helpers.Response(c, http.StatusOK, "Your photo has been successfully deleted", nil)
}
