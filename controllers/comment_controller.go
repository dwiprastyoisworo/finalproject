package controllers

import (
	"finalProject/helpers"
	"finalProject/models"
	"finalProject/services"
	"net/http"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type CommentController struct {
	commentService services.ICommentService
}

func NewCommentController(commentService services.ICommentService) *CommentController {
	return &CommentController{commentService: commentService}
}

func (s *CommentController) Create(c *gin.Context) {
	userData := c.MustGet("userData").(jwt.MapClaims)
	floatUserId := userData["id"].(float64)
	userId := uint(floatUserId)

	var comment models.Comment

	if err := c.ShouldBindJSON(&comment); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	err := s.commentService.Create(userId, &comment)
	if err != nil {
		helpers.Response(c, http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized), nil)
		return
	}
	helpers.Response(c, http.StatusOK, http.StatusText(http.StatusOK), comment.CommentToCreateCommentResponse())
}

func (s *CommentController) Get(c *gin.Context) {
	userData := c.MustGet("userData").(jwt.MapClaims)
	floatUserId := userData["id"].(float64)
	userId := uint(floatUserId)

	var comments []*models.Comment
	err := s.commentService.Get(&userId, comments)
	if err != nil {
		helpers.Response(c, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		return
	}
	var responseComments []models.CommentResponse
	for _, k := range comments {
		responseComments = append(responseComments, k.CommentToCommentDetailResponse())
	}

	helpers.Response(c, http.StatusOK, http.StatusText(http.StatusOK), responseComments)
}

func (s *CommentController) Update(c *gin.Context) {
	userData := c.MustGet("userData").(jwt.MapClaims)
	floatUserId := userData["id"].(float64)
	userId := uint(floatUserId)
	id, err := strconv.ParseFloat(c.Param("commentId"), 64)
	commentId := uint(id)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}
	var comment models.Comment

	if err := c.ShouldBindJSON(&comment); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	comment, err = s.commentService.Update(&userId, &commentId, &comment)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}
	helpers.Response(c, http.StatusOK, http.StatusText(http.StatusOK), comment.CommentToUpdateCommentResponse())
}

func (s *CommentController) Delete(c *gin.Context) {
	userData := c.MustGet("userData").(jwt.MapClaims)
	floatUserId := userData["id"].(float64)
	userId := uint(floatUserId)

	tmpCommentId, err := strconv.ParseFloat(c.Param("commentId"), 64)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}
	commentId := uint(tmpCommentId)

	err = s.commentService.Delete(&userId, &commentId)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}
	helpers.Response(c, http.StatusOK, http.StatusText(http.StatusOK), nil)
}
