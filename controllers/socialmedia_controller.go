package controllers

import (
	"finalProject/helpers"
	"finalProject/models"
	"finalProject/services"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type SocialmediaController struct {
	socialmediaService services.ISocialmediaService
}

func NewSocialmediaController(socialmediaService services.ISocialmediaService) *SocialmediaController {
	return &SocialmediaController{socialmediaService: socialmediaService}
}

func (s *SocialmediaController) Create(c *gin.Context) {
	userData := c.MustGet("userData").(jwt.MapClaims)
	floatUserId := userData["id"].(float64)
	userId := uint(floatUserId)

	var socialmedia = models.Socialmedia{}

	if err := c.ShouldBindJSON(&socialmedia); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	err := s.socialmediaService.Create(userId, &socialmedia)

	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}

	helpers.Response(c, http.StatusOK, http.StatusText(http.StatusOK), socialmedia.SocialmediaToCreateSocialmediaResponse())
}

func (s *SocialmediaController) Get(c *gin.Context) {
	userData := c.MustGet("userData").(jwt.MapClaims)
	floatUserId := userData["id"].(float64)
	userId := uint(floatUserId)

	var socialmedias []models.Socialmedia
	err := s.socialmediaService.Get(userId, &socialmedias)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}

	var socialmediaResponse = []models.SocialmediaResponse{}
	for _, k := range socialmedias {
		socialmediaResponse = append(socialmediaResponse, k.SocialmediaToSocialmediaResponse())
	}

	helpers.Response(c, http.StatusOK, http.StatusText(http.StatusOK), socialmediaResponse)
}

func (s *SocialmediaController) Update(c *gin.Context) {
	userData := c.MustGet("userData").(jwt.MapClaims)
	floatUserId := userData["id"].(float64)
	userId := uint(floatUserId)

	socialmediaId, err := strconv.ParseFloat(c.Param("socialmediaId"), 64)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}
	id := uint(socialmediaId)
	var socialmedia = models.Socialmedia{}
	if err := c.ShouldBindJSON(&socialmedia); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	socialmedia, err = s.socialmediaService.Update(userId, id, &socialmedia)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}

	helpers.Response(c, http.StatusOK, http.StatusText(http.StatusOK), socialmedia.SocialmediaToUpdateSocialmediaResponse())
}

func (s *SocialmediaController) Delete(c *gin.Context) {
	userData := c.MustGet("userData").(jwt.MapClaims)
	floatUserId := userData["id"].(float64)
	userId := uint(floatUserId)

	socialmediaId, err := strconv.ParseFloat(c.Param("socialmediaId"), 64)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}
	id := uint(socialmediaId)

	err = s.socialmediaService.Delete(userId, id)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}
	helpers.Response(c, http.StatusOK, "Your socialmedia has been successfully deleted", nil)
}
