package controllers

import (
	"finalProject/helpers"
	"finalProject/models"
	"finalProject/services"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type UserController struct {
	userService services.IUserService
}

func NewUserController(userService services.IUserService) *UserController {
	return &UserController{userService: userService}
}

func (s *UserController) Register(c *gin.Context) {
	var user models.User

	if err := c.ShouldBindJSON(&user); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	if user.Age <= 8 {
		helpers.Response(c, http.StatusBadRequest, "Age must be more than 8", nil)
		return
	}
	data, err := s.userService.Register(&user)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}
	helpers.Response(c, http.StatusOK, http.StatusText(http.StatusOK), data.UserToUser())
}

func (s *UserController) Login(c *gin.Context) {
	var user models.User

	if err := c.ShouldBindJSON(&user); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	token, err := s.userService.Login(&user)
	if err != nil {
		helpers.Response(c, http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized), nil)
		return
	}

	var mapResp = map[string]string{
		"token": token,
	}
	helpers.Response(c, http.StatusOK, http.StatusText(http.StatusOK), mapResp)
}

func (s *UserController) Update(c *gin.Context) {
	var user models.User
	UserId, err := strconv.ParseFloat(c.Param("userId"), 64)
	uintId := uint(UserId)
	fmt.Println(uintId)
	if err := c.ShouldBindJSON(&user); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	responseUser, err := s.userService.Update(uintId, &user)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}

	helpers.Response(c, http.StatusOK, http.StatusText(http.StatusOK), responseUser.UserToUser())
}

func (s *UserController) Delete(c *gin.Context) {
	userData := c.MustGet("userData").(jwt.MapClaims)
	floatUserId := userData["id"].(float64)
	userId := uint(floatUserId)

	err := s.userService.Delete(userId)
	if err != nil {
		helpers.Response(c, http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
		return
	}
	helpers.Response(c, http.StatusOK, "Your account has been successfully deleted", nil)
}
