package routers

import (
	"finalProject/controllers"
	"finalProject/middleware"
	"finalProject/repositories"
	"finalProject/services"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func StartServer(db *gorm.DB, router *gin.Engine) {

	userRepository := repositories.NewUserRepository()
	userService := services.NewUserService(userRepository, db)
	userController := controllers.NewUserController(userService)

	photoRepository := repositories.NewPhotoRepository()
	photoService := services.NewPhotoService(userRepository, photoRepository, db)
	photoController := controllers.NewPhotoController(photoService)

	socialmediaRepository := repositories.NewSocialmediaRepository()
	socialmediaService := services.NewSocialmediaService(userRepository, socialmediaRepository, db)
	socialmediaController := controllers.NewSocialmediaController(socialmediaService)

	commentRepository := repositories.NewCommentRepository()
	commentService := services.NewCommentService(userRepository, commentRepository, db)
	commentController := controllers.NewCommentController(commentService)

	userRouter := router.Group("/users")
	{
		userRouter.POST("/register", userController.Register)
		userRouter.POST("/login", userController.Login)
		userRouter.PUT("/:userId", middleware.JWTAuth(), userController.Update)
		userRouter.DELETE("/", middleware.JWTAuth(), userController.Delete)
	}

	photoRouter := router.Group("/photos")
	{
		photoRouter.POST("/", middleware.JWTAuth(), photoController.Create)
		photoRouter.GET("/", middleware.JWTAuth(), photoController.Get)
		photoRouter.PUT("/:photoId", middleware.JWTAuth(), photoController.Update)
		photoRouter.DELETE("/:photoId", middleware.JWTAuth(), photoController.Delete)
	}

	commentRouter := router.Group("/comments")
	{
		commentRouter.POST("/", middleware.JWTAuth(), commentController.Create)
		commentRouter.GET("/", middleware.JWTAuth(), commentController.Get)
		commentRouter.PUT("/:commentId", middleware.JWTAuth(), commentController.Update)
		commentRouter.DELETE("/:commentId", middleware.JWTAuth(), commentController.Delete)
	}

	socialmediaRouter := router.Group("/socialmedias")
	{
		socialmediaRouter.POST("/", middleware.JWTAuth(), socialmediaController.Create)
		socialmediaRouter.GET("/", middleware.JWTAuth(), socialmediaController.Get)
		socialmediaRouter.PUT("/:socialmediaId", middleware.JWTAuth(), socialmediaController.Update)
		socialmediaRouter.DELETE("/:socialmediaId", middleware.JWTAuth(), socialmediaController.Delete)
	}

}
