package config

import (
	"finalProject/models"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
)

func DBInit() *gorm.DB {

	dsn := "root:root@tcp(127.0.0.1:3306)/golang?charset=utf8&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Error connect to database :", err)
	}

	db.AutoMigrate(
		models.User{},
		models.Photo{},
		models.Socialmedia{},
		models.Comment{},
	)

	return db
}
