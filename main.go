package main

import (
	"finalProject/config"
	"finalProject/routers"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"log"
	"os"
)

func main() {
	err := godotenv.Load()

	if err != nil {
		log.Fatal("Error loading .env file")
	}
	var db = config.DBInit()
	router := gin.Default()
	routers.StartServer(db, router)
	router.Run(os.Getenv("PORT"))
}
